// .clearForm - clears form fields from any values
$.fn.clearForm = function () {
    var el = this.find('input, select, textarea'),
        len = el.length,
        i = 0;
    for (; i < len; i++) {
        switch (el[i].type) {
            case 'text':
            case 'password':
            case 'select-one':
            case 'select-multiple':
            case 'textarea':
            case 'email':
            case 'date':
            case 'number':
            case 'phone':
                $(el[i]).val('');
                break;
            case 'checkbox':
            case 'radio':
                el[i].checked = false;
        }
    }
}